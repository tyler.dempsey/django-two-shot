from django.shortcuts import render
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Receipt


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    context_object_name = "receipt"
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipt_list.html"
